//import express
const express = require('express');
//importing mongoose
const mongoose = require('mongoose');

//conecction to mongoose atalas
 mongoose.connect("mongodb+srv://admin:admin@cluster0.aydw0.mongodb.net/dslcommerce?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

// //database connection confirmation message
 mongoose.connection.once('open', () => console.log("Now connected to MongoDB Atlas."))


//server setup
const app = express();

//middlewear that allows our app to receive nested JSON data
app.use(express.json())
app.use(express.urlencoded({
	extended: true
}))

app.get('/', (req, res)=>{
	res.send("DSLcommerce working")
	console.log('')
})


let port = 3000;
app.listen(port, ()=>{
	console.log('listening on port 3000');
})