const express = require('express');
const router = express.Router();
const userController = require('../controllers/user');


//route for checking if an email exists in our database
router.post("/checkEmail", (req, res)=>{
	userController.checkEmail(req.body).then(resultFromController => res.send(resultFromController));
})

//route for user registration
router.post("/register", (req, res)=>{
	//console.log(req.body);
	userController.register(req.body).then(resultFromController => res.send(resultFromController))
})

//route for login
router.post("/login", (req, res)=>{
	userController.login(req.body).then(resultFromController => res.send(resultFromController))
})

//route for getting user profile
router.get("/details", auth.verify, (req, res)=> {
	//get users ID from token
	const userId = auth.decode(req.headers.authorization).id;

	userController.getProfile(userId).then(resultFromController => res.send(resultFromController))
})


module.exports = router;