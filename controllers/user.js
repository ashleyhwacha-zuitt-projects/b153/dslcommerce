const User = require('../models/user');
const bcrypt = require('bcrypt');
const auth = require('../auth');


module.exports.checkEmail = (body) =>{
	return User.find({email: body.email}).then(result=>{
		if(result.length > 0){
			return true; //meaning email exists
		}else{
			return false;
		}
	})
}

module.exports.register = (body) => {
	let newUser = new User({
		fisrtName: body.firstName,
		lastName : body.lastname,
		email: body.email,
		password: bcrypt.hashSync(body.password, 10),
		mobileNo: body.mobileNo
	})

	return newUser.save().then((user, error)=> {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}
